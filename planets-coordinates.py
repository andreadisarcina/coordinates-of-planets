import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from OEtoSTATE import *

#initial time is 1st January 2021, 23:00 UT
day=1
month=1
year=2021
hour=23.

#conversion of Gregorian date in Julian day (at 12:00 UT)
def julian(d,m,y):
	temp1=m-14
	temp2=d-32075+int(1461*(y+4800+int(temp1/12))/4)
	temp3=int(temp1/12)*12
	temp4=int(y+4900+int(temp1/12))/100
	jd=temp2+int(367*(m-2-temp3)/12)-int(3*temp4/4)
	return jd
jul=julian(day,month,year)            #corresponding Julian day
fraction=(hour-12.)/24.				  #fraction of the day corresponding to given hour
t0=jul+fraction                       #initial Julian date
JD=[t0]
if(year%4==0.):                       #leap year    
	n=366
else:                                 #not leap year
	n=365
while(len(JD)<n):                   
	JD.append(JD[-1]+1.)
JD=np.array(JD)
#now JD contains all Julian dates corresponding to every day in 2021 at 23:00 UT
T=(JD-2451545.)/36525           #time in Julian centuries

#Orbital elements coefficients (in the mean ecliptic and equinox of date reference system):
Mercury=np.array([[0.387098310,0.,0.,0.],                                       #a 
				  [0.20563175,+0.000020407,-0.0000000283,-0.00000000018],       #e
				  [7.004986,+0.0018215,-0.00001810,+0.000000056],               #i
				  [48.330893,+1.1861883,+0.00017542,+0.000000215],              #Omega (W)
				  [77.456119,+1.5564776,+0.00029544,+0.000000009],              #w tilde (pi)
				  [252.250906,+149474.0722491,+0.00030350,+0.000000018]])       #L            

Venus=np.array([[0.723329820,0.,0.,0.],                                         #a
				[0.00677192,-0.000047765,+0.0000000981,+0.00000000046],         #e
				[3.394662,+0.0010037,-0.00000088,-0.000000007],                 #i
				[76.679920,+0.9011206,+0.00040618,-0.000000093],                #Omega (W)
				[131.563703,+1.4022288,-0.00107618,-0.000005678],               #w tilde (pi)
				[181.979801,+58519.2130302,+0.00031014,+0.000000015]])          #L 

Mars=np.array([[1.523679342,0.,0.,0.],                                          #a
	           [0.09340065,+0.000090484,-0.0000000806,-0.00000000025],          #e
	           [1.849726,-0.0006011,+0.00001276,-0.000000007],                  #i
	           [49.558093,+0.7720959,+0.00001557,+0.00000267],                  #Omega (W)
	           [336.060234,+1.8410449,+0.00013477,+0.000000536],                #w tilde (pi)
	           [355.433000,+19141.6964471,+0.00031052,+0.000000016]])           #L

Earth=np.array([[1.000001018,0.,0.,0.],                                         #a
	            [0.01670863,-0.000042037,-0.0000001267,+0.00000000014],         #e
	            [0.,0.,0.,0.],                                                  #i
	            [0.,0.,0.,0.],                                                  #Omega (W)
	            [102.937348,+1.7195366,+0.00045688,-0.000000018],               #w tilde (pi)
				[100.466457,+36000.7698278,+0.00030322,+0.000000020]])          #L
#a is in AU, while i,W,pi,L are in deg
#gravitational parameter of the Sun (neglecting the mass of all the planets) in km^3/s^2
mu=1.3271244e11

Mercury_file=open('Mercury.txt','w')
Venus_file=open('Venus.txt','w')
Mars_file=open('Mars.txt','w')
Mercury_file.write('%   Julian Date           X (km)           Y (km)           Z (km)           Right Ascension (deg)           Declination (deg)\n')
Venus_file.write('%   Julian Date           X (km)           Y (km)           Z (km)           Right Ascension (deg)           Declination (deg)\n')
Mars_file.write('%   Julian Date           X (km)           Y (km)           Z (km)           Right Ascension (deg)           Declination (deg)\n')

#loop through time instants:
for i in range(len(JD)):

	#orbital elements calculation for i-th time instant
	OEMercury=Mercury[:,0]+Mercury[:,1]*T[i]+Mercury[:,2]*T[i]**2.+Mercury[:,3]*T[i]**3.
	OEVenus=Venus[:,0]+Venus[:,1]*T[i]+Venus[:,2]*T[i]**2.+Venus[:,3]*T[i]**3.
	OEMars=Mars[:,0]+Mars[:,1]*T[i]+Mars[:,2]*T[i]**2.+Mars[:,3]*T[i]**3.
	OEEarth=Earth[:,0]+Earth[:,1]*T[i]+Earth[:,2]*T[i]**2.+Earth[:,3]*T[i]**3.

	#conversion of orbital elements at i-th time instant to dynamical states (positions in km and velocities in km/s) of the plantes at the same time instant relative to the heliocentric ecliptic reference system
	StateMercuryHelioEclip=oetostate(OEMercury[0],OEMercury[1],OEMercury[2],OEMercury[3],OEMercury[4],OEMercury[5],mu)
	StateVenusHelioEclip=oetostate(OEVenus[0],OEVenus[1],OEVenus[2],OEVenus[3],OEVenus[4],OEVenus[5],mu)
	StateMarsHelioEclip=oetostate(OEMars[0],OEMars[1],OEMars[2],OEMars[3],OEMars[4],OEMars[5],mu)
	StateEarthHelioEclip=oetostate(OEEarth[0],OEEarth[1],OEEarth[2],OEEarth[3],OEEarth[4],OEEarth[5],mu)

	#dynamical state from heliocentric ecliptic system to geocentric ecliptic system:
	StateMercuryGeoEclip=StateMercuryHelioEclip-StateEarthHelioEclip
	StateVenusGeoEclip=StateVenusHelioEclip-StateEarthHelioEclip
	StateMarsGeoEclip=StateMarsHelioEclip-StateEarthHelioEclip

	#position vectors (in km) of the planets at i-th time instant relative to the geocentric ecliptic reference system
	RMercuryGeoEclip=StateMercuryGeoEclip[:3]
	RVenusGeoEclip=StateVenusGeoEclip[:3]
	RMarsGeoEclip=StateMarsGeoEclip[:3]

	#mean obliquity (in deg) at i-th time instant:
	epsilon=23.439291-0.0130042*T[i]-0.00059*T[i]**2.+0.001813*T[i]**3.
	#mean obliquity in rad:
	epsilon=epsilon/180.*np.pi

	#rotation matrix from ecliptic to equatorial reference frames:
	R1=np.array([[1.,0.,0.],
		         [0.,np.cos(-epsilon),np.sin(-epsilon)],
		         [0.,-np.sin(-epsilon),np.cos(-epsilon)]])

	#conversion of Cartesian coordinates (in km) from ecliptic geocentric to equatorial geocentric:
	RMercuryGeoEquat=np.dot(R1,RMercuryGeoEclip)
	RVenusGeoEquat=np.dot(R1,RVenusGeoEclip)
	RMarsGeoEquat=np.dot(R1,RMarsGeoEclip)

	#calculation of right ascension (RA) and declination (D) at i-th time instant (in rad):
	RAMercury=np.arctan(RMercuryGeoEquat[1]/RMercuryGeoEquat[0])
	DMercury=np.arctan(RMercuryGeoEquat[2]/np.sqrt(RMercuryGeoEquat[0]**2.+RMercuryGeoEquat[1]**2.))
	RAVenus=np.arctan(RVenusGeoEquat[1]/RVenusGeoEquat[0])
	DVenus=np.arctan(RVenusGeoEquat[2]/np.sqrt(RVenusGeoEquat[0]**2.+RVenusGeoEquat[1]**2.))
	RAMars=np.arctan(RMarsGeoEquat[1]/RMarsGeoEquat[0])
	DMars=np.arctan(RMarsGeoEquat[2]/np.sqrt(RMarsGeoEquat[0]**2.+RMarsGeoEquat[1]**2.))
	#conversion of right ascension and declination in deg:
	RAMercury=RAMercury*180./np.pi
	DMercury=DMercury*180./np.pi
	RAVenus=RAVenus*180./np.pi
	DVenus=DVenus*180./np.pi
	RAMars=RAMars*180./np.pi
	DMars=DMars*180./np.pi

	#This definition of the right ascension is ambiguous (and it produces discontinuities), since RA is defined in a 360° interval (for example [-180°,180°]), so there always exists a pair of angles (separated by 180°) whose tangent is the same.
	#This is not the case of declination, since D is defined in the interval [-90°,90°], and in that interval there is a one to one correspondace between angles and tangents.
	#To deal with RA ambiguity we just have to consider the sign of the coordinates. The function numpy.arctan returns the angle in the [-90°,90°] interval.
	#If x>=0 then RA is in [-90°,90°] (and we don't do nothing). If x<0 then RA is outside of [-90°,90°], so if RA<=0 we consider RA=RA+180°, while if RA>0 we consider RA=RA-180°.
	if(RMercuryGeoEquat[0]<0.):
		if(RAMercury<=0.):
			RAMercury+=180.
		else:
			RAMercury-=180.
	if(RVenusGeoEquat[0]<0.):
		if(RAVenus<=0.):
			RAVenus+=180.
		else:
			RAVenus-=180.
	if(RMarsGeoEquat[0]<0.):
		if(RAMars<=0.):
			RAMars+=180.
		else:
			RAMars-=180.

	#write txt files containing the values of geocentric Cartesian equatorial coordinates in km and right ascension and declination in deg for all time instants considered:
	Mercury_file.write('    '+str(JD[i])+'           '+str(RMercuryGeoEquat[0])+'           '+str(RMercuryGeoEquat[1])+'           '+str(RMercuryGeoEquat[2])+'           '+str(RAMercury)+'           '+str(DMercury)+'\n')
	Venus_file.write('    '+str(JD[i])+'           '+str(RVenusGeoEquat[0])+'           '+str(RVenusGeoEquat[1])+'           '+str(RVenusGeoEquat[2])+'           '+str(RAVenus)+'           '+str(DVenus)+'\n')
	Mars_file.write('    '+str(JD[i])+'           '+str(RMarsGeoEquat[0])+'           '+str(RMarsGeoEquat[1])+'           '+str(RMarsGeoEquat[2])+'           '+str(RAMars)+'           '+str(DMars)+'\n')

	#do some plots for the planets
	plt.scatter(RAMercury,DMercury,color='tab:brown',s=0.5)
	plt.scatter(RAVenus,DVenus,color='tab:orange',s=0.5)
	plt.scatter(RAMars,DMars,color='r',s=0.5)
	if(i==len(JD)-1):
		plt.scatter(RAMercury,DMercury,color='tab:brown',s=10,label='Mercury')
		plt.scatter(RAVenus,DVenus,color='tab:orange',s=10,label='Venus')
		plt.scatter(RAMars,DMars,color='r',s=10,label='Mars')
plt.scatter(0.,0.,color='k',s=5,label='Vernal point')
plt.hlines(0.,-180.,180.,'k',lw=0.5,label='Equator')
plt.vlines(0.,-90.,90.,'k',linestyles='dashed',lw=0.5)
plt.legend()
plt.xlabel('Right Ascension (deg)')
plt.ylabel('Declination (deg)')
plt.xlim(180.,-180.)                 #x axis is inverted since right ascension increases toward East, that is going from right to left when looking at the sky from the Earth
plt.ylim(-90.,90.)
plt.show()