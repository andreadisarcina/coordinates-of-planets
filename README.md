# Coordinates of Planets

The script is a function useful to compute the geocentric Cartesian equatorial coordinates and the right ascension and declination of Mercury, Venus and Mars for specified dates (in the script, each day in 2021 at 23:00 UT) and to plot the path of the planets on the plane of the sky (i.e., in declination vs. right ascension).

It uses the function OEtoSTATE to convert the orbit elements into the Cartesian dynamical state.

The txt files contain the outputs of the script (geocentric Cartesian equatorial coordinates, right ascension and declination for each planet fot each date).

The plot DvsRA.pdf, produced by the script, shows the path of the planets on the sky.
